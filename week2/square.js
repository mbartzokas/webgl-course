"use strict";

var gl;
var points;

window.onload = function init()
{
    var canvas = document.getElementById( "gl-canvas" );

    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }

    // Four Vertices

    var vertices = [
      -0.5, -0.5,
        -0.5,  0.5,
        0.5, 0.5,
        0.5, -0.5
    ];

    //
    //  Configure WebGL
    //
    gl.viewport( 0, 0, // start position
        canvas.width,
        canvas.height );
    gl.clearColor( 0, 0, 0, //black on RGB
        1.0 );

    //  Load shaders and initialize attribute buffers

    var program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Load the data into the GPU

    var bufferId = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW );

    // Associate out shader variables with our data buffer

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 2, // dimensions
        gl.FLOAT,
        false, // don't normalize the data
        0,// offset parameter
        0 // how far apart are the value data
        );
    gl.enableVertexAttribArray( vPosition );// if we have many buffers this is to


    render();
};


function render() {
    gl.clear( gl.COLOR_BUFFER_BIT );
    gl.drawArrays( gl.TRIANGLE_FAN, 0, 4 // number of vertices
    );
    //gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
    //gl.drawArrays(g.TRIANGLES, 0, 6);
}
