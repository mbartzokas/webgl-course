"use strict";

var tessellationDegree = 5;

window.onload = function init() {
    var triangle = [vec2(-1, -1), vec2(0, 1), vec2(1, -1)];
    var approach = new RecursiveSubdivisionApproach(triangle);
    approach.tessellate();
    var webGl = initializeWebGlAndSendPointsDataToGpu(approach.points);
    approach.render(webGl);
};

function IterativeApproach(triangle) {
    this.points = [];

    this.tessellate = function() {
        var fraction = 1 / tessellationDegree;

        for (var r = 0; r < triangle.length; r++) {
            var a = triangle[r];
            var b = triangle[(r + 1) % 3];
            var c = triangle[(r + 2) % 3];

            for (var k = 0; k < tessellationDegree; k++) {
                var abXi = a[0] + (fraction * k) * (b[0] - a[0]);
                var abYi = a[1] + (fraction * k) * (b[1] - a[1]);

                var cbXi = c[0] + (fraction * k) * (b[0] - c[0]);
                var cbYi = c[1] + (fraction * k) * (b[1] - c[1]);

                this.points.push(vec2(abXi, abYi), vec2(cbXi, cbYi));
            }
        }
    };

    this.render = function(webGl) {
        webGl.clear(webGl.COLOR_BUFFER_BIT);
        webGl.drawArrays(webGl.LINES, 0, this.points.length);
    };
};

function RecursiveSubdivisionApproach(triangle) {
    this.points = [];
    
    this.tessellate = function() {
        devideTriangleIntoFour(triangle[0], triangle[1], triangle[2], tessellationDegree, this.points);
        function devideTriangleIntoFour(v1, v2, v3, tessellationDegree, points){
            if(tessellationDegree == 0) {
                points.push(v1, v2, v3);            
            } else {
                var m1 = median(v1, v2);
                var m2 = median(v2, v3);
                var m3 = median(v3, v1);

                devideTriangleIntoFour(v1, m1, m3, tessellationDegree - 1, points);
                devideTriangleIntoFour(m1, v2, m2, tessellationDegree - 1, points);
                devideTriangleIntoFour(m3, m2, v3, tessellationDegree - 1, points);
                devideTriangleIntoFour(m1, m2, m3, tessellationDegree - 1, points);
            }
        };

        function median(v1, v2) {
            return vec2((v1[0] + v2[0]) / 2, (v1[1] + v2[1]) / 2);
        };
    };

    this.render = function(webGl) {
        webGl.clear(webGl.COLOR_BUFFER_BIT);
        webGl.drawArrays(webGl.TRIANGLES, 0, this.points.length);
    };            
};

function initializeWebGlAndSendPointsDataToGpu(points) {
    var canvas = document.getElementById("gl-canvas");

    var webGl = WebGLUtils.setupWebGL(canvas);
    if (!webGl) {
        alert("WebGL isn't available");
    }

    //  Configure WebGL

    webGl.viewport(0, 0, canvas.width, canvas.height);
    webGl.clearColor(1, 1, 1, 1);

    //  Load shaders and initialize attribute buffers

    var program = initShaders(webGl, "vertex-shader", "fragment-shader");
    webGl.useProgram(program);

    // Load the data into the GPU

    var bufferId = webGl.createBuffer();
    webGl.bindBuffer(webGl.ARRAY_BUFFER, bufferId);
    webGl.bufferData(webGl.ARRAY_BUFFER, flatten(points), webGl.STATIC_DRAW);

    // Associate out shader variables with our data buffer

    var vPosition = webGl.getAttribLocation(program, "vPosition");
    webGl.vertexAttribPointer(vPosition, 2, webGl.FLOAT, false, 0, 0);
    webGl.enableVertexAttribArray(vPosition);

    return webGl;
}
